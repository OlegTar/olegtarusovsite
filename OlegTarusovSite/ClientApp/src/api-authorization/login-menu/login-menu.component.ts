import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '../authorize.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
//import { UserManager, OidcClient, OidcClientSettings } from 'oidc-client';

@Component({
  selector: 'app-login-menu',
  templateUrl: './login-menu.component.html',
  styleUrls: ['./login-menu.component.css']
})
export class LoginMenuComponent implements OnInit {
  public isAuthenticated: Observable<boolean>;
  public userName: Observable<string>;

  constructor(private authorizeService: AuthorizeService) { }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.name));
  }

  //async msLogin() {
  //  const settings: OidcClientSettings = {
  //    client_id: '0bdfd3f2-9023-4289-8cbc-323a7c00d4db',
  //    client_secret: 'YTa7Q~bZTwMTOS1Qu.QsuIX-PI.sh9rnri~FX',
  //    redirect_uri: 'https://localhost:44358/signin-microsoft',
  //    scope: "email openid profile",
  //    response_type: "id_token token",
  //    authority: "https://login.microsoftonline.com/728614cf-9686-41e9-a18d-02d191c3e37e",
      
  //  };
  //  const oidcClient = new OidcClient(settings);
  //  const user = new UserManager(settings);
  //  const request = await user.createSigninRequest({ state: 'test' });
  //  console.log(request);
  //}
}
